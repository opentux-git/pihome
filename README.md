# PiHome
![PiHome Demo based on OpenHAB](https://opentux.cz/sites/default/files/gitlab_header_0.png)


## Getting started

Download our Ready To Use preconfigurated images based or OpenHAB or Home Assistant. If you do not have any experience with these projects we recommend to use OpenHAB for simplicity and steep learning curve.

After download image, insert SD card from Raspberry Pi (16GB or more) or SSD disc and use [Etcher](https://etcher.balena.io/) or [Raspberry Pi Imager](https://www.raspberrypi.com/software/) to upload your image to the SD card or SSD disc.

After finish, insert SD card or connect SSD to USB of the Raspberry Pi 4, connect network cable and boot it. Your Raspberry should get IP address from your router. Check it and paste address to your browser:

http://yourraspberryip:8080

Wuala! You should be able to see demo dashboard. Default login to open admin features are:
L: opentux
P: opentux

Default password for SSH:
L:openhabian
P:openhabian

## Images

PiHome OpenHAB English Edition (Compatible with Raspberry Pi 5, 4, 3)

https://webshare.cz/#/file/Z8797AJjRP

PiHome OpenHAB Czech Edition: (Compatible with Raspberry Pi 4, 3)

https://webshare.cz/#/file/mLZhzJucuF

PiHome Home Assistant:

TBD


## Usage

Extract image to your Raspberry Pi (see Getting Started). Boot up and customize based on your needs. Principle and hardware with Arduino sketch is described on our [web pages.](https://opentux.eu/solutions/smart-home/how-to?id=39) 

Feel free to use and we will be very glad to present your own solution and customizated project in our [open realization section](https://opentux.eu/solutions/smart-home/open-source-realizations). Just [register](https://opentux.eu/user/register) and you will able to fill the realization form to share with other users.

## Support
For step by step tutorials visit https://opentux.eu

We are providing payed remote support for similar solution to helps you configure environment at your home.

Email: info@opentux.eu

Language: English, Czech, Slovak

## PiHome Hardware example

![PiHome Wiring Scheme](https://opentux.cz/sites/default/files/pihome_scheme_model_1.jpg)



## Screenshots

![PiHome Demo based on OpenHAB](https://opentux.cz/sites/default/files/phone_home_en.png)

